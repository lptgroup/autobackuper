<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class BackupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:backup {--clean}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup all the sites';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    protected $sites = [
        [
            'domain' => 'n4doc.pro-linuxpl.com',
            'database' => 'n4doc_tor',
            'paths' => ['n4doc.pro-linuxpl.com/storage/app/public'],
        ],
        [
            'domain' => 'pto15.pro-linuxpl.com',
            'database' => 'pto15_pto2',
            'paths' => ['pto15.pro-linuxpl.com/storage/app/public'],
        ],
        [
            'domain' => 'ptoitr15.pro-linuxpl.com',
            'database' => 'ptoitr15_ptoitr2',
            'paths' => ['ptoitr15.pro-linuxpl.com/storage/app/public'],
        ],
    ];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach ($this->sites as $site) {
            $this->setupConfigForSite($site);
            $this->callBackupCommand();
        };
    }

    protected function callBackupCommand()
    {
        $this->call('backup:run');
    }

    protected function setupConfigForSite($site)
    {
        \Config::set('backup.backup.name', $this->siteName($site));

        \Config::set('database.connections.pgsql.database', $site['database']);

        \Config::set('backup.backup.source.files.include', $this->siteIncludes($site));
    }

    protected function siteName($site)
    {
        return 'http://'.$site['domain'];
    }

    protected function siteIncludes($site)
    {
        return array_map(function ($path) use ($site) {
            return base_path("../{$site['domain']}/$path");
        }, $site['paths']);
    }
}
